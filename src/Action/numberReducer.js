export const numberReducer = (state = 0, action) => {
    switch (action.type) {
        case 'ADD':
            return state + 1
        case 'ADD2':
            return state + action.number
        case 'MINUS':
            return state - 1
        case 'MINUS2':
            return state - action.number
        case 'RESET':
            return 0
        default:
            return state
    }
}