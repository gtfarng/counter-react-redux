import React, { Component } from 'react';
import { add, add2, minus, minus2, reset, set, store } from '../App'
import { connect } from 'react-redux'

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {
            number: ''
        }
        this.onChangeNumber = this.onChangeNumber.bind(this)
    }

    onChangeNumber = (e) => {
        this.setState({
            number: e.target.value
        })
    }
    render() {
        return (
            <div className="container" style={{ margin: '20px' }}>
                <h1>Counter: {this.props.number} <br /></h1><br />
                <button class="btn btn-primary" onClick={this.props.add}>Up</button>
                <button class="btn btn-success" onClick={this.props.minus}>Down</button>
                <button class="btn btn-danger" onClick={this.props.reset}>Reset</button>
                <button class="btn btn-primary" onClick={() => store.dispatch(add2(2))}>Up+2</button>
                <button class="btn btn-success" onClick={() => store.dispatch(minus2(2))}>Down-2</button><br /><br />

                <input onChange={this.onChangeNumber} value={this.state.number} type="number" placeholder="Enter number" />
                <button class="btn btn-warning" type="button" onClick={() => { store.dispatch(set(parseInt(this.state.number))) }} > SET </button><br />

            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return { number: state.number }
}

const mapDispatchToProps = (dispatch) => {
    return {
        add: () => dispatch(add()),
        minus: () => dispatch(minus()),
        reset: () => dispatch(reset()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Count);

